import {useState, useEffect, useContext, Fragment} from 'react';
import {Container, Card, Row, Col, Button, Stack} from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import EditIcon from '@mui/icons-material/Edit';
import UserContext from '../../UserContext';
import { FormControl, MenuItem, InputLabel, Select } from "@mui/material";

export default function AddToCart() {

	const { user } = useContext(UserContext);

	const navigate = useNavigate(); 

	//The "useParams" allows us to retrieve the courseId passed via the URL
	const { productId } = useParams()
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [author, setAuthor] = useState("");
	const [publishedOn, setPublishedOn] = useState("");
	const [price, setPrice] = useState(0);
	const [products, setProducts] = useState([]);
	const [quantity, setQuantity] = useState(1);

    useEffect(() => {
        fetch(`https://secure-castle-27864.herokuapp.com/products/${productId}`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            setProducts(data)
        })
    });	


    function addToCart(productId) {
        if (products.stocks > 0) {
            fetch(`https://secure-castle-27864.herokuapp.com/users/${productId}/cart`, {
                method: 'POST',
                headers: {
                    Authorization: `Bearer ${localStorage.getItem('token')}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    quantity: quantity
                })
            })
            .then(res => res.json())
            .then(data => {
                if (data.productAdded) {
                    Swal.fire({
                        title: "Item added to cart!", 
                        icon: "success",
                        text: "You have successfully added the item!"
                    })
                } else if (data.auth === "failed") {
                    Swal.fire({
                        title: 'You are not logged in!',
                        text: "Please login to add something to your cart",
                        icon: 'error',
                        showCancelButton: true,
                        confirmButtonColor: '#4b5320',
                        cancelButtonColor: '#990f02',
                        confirmButtonText: 'Go to Login'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            navigate("/login")
                        }
                    })
                } else {
                    Swal.fire({
                        title: "Something went wrong!",
                        icon: "error", 
                        text: "Please try again."
                    })
                }
            })
        } else {
            Swal.fire({
                title: "Sorry! Product already out of stock",
                icon: "error", 
                text: "Updating your page..."
            })
        }
    }

	useEffect(() => {

		//console.log(productId);
		fetch(`https://secure-castle-27864.herokuapp.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setName(data.name);
			setDescription(data.description)
			setPrice(data.price);
			setAuthor(data.author);
			setPublishedOn(data.publishedOn);

		})

	},[productId])

	function login(e) {
		e.preventDefault()
		navigate("/login")
	}


	return(
		<Row >
		<Card.Header as="h5" className="text-center mt-3 mb-3">{name}</Card.Header>
		<Stack direction="horizontal" gap={3}  className="mt-7 mb-3">
				<div sm={6} md={6}>
			
			
					<Card.Body>
						<Card.Title></Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>

							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>

              <FormControl fullWidth>
                  <InputLabel id="quantity">Qty</InputLabel>
                  <Select
                      labelId="quantity"
                      id="quantity"
                      value={quantity}
                      label="Qty"
                      onChange={(e) => setQuantity(e.target.value)}
                  >
              <MenuItem value={1}>1</MenuItem>
              <MenuItem value={2}>2</MenuItem>
              <MenuItem value={3}>3</MenuItem>
              <MenuItem value={4}>4</MenuItem>
              <MenuItem value={5}>5</MenuItem>
              <MenuItem value={6}>6</MenuItem>
              <MenuItem value={7}>7</MenuItem>
          </Select>
      </FormControl>                  
						{
							(user.id !== null) ?
							<Fragment>
								{ 
									(user.isAdmin === false) ?
								<Button variant="outline-success" onClick={() => addToCart(productId)}><ShoppingCartIcon />Add to Cart</Button>
								:
									<Button variant="primary" as={Link} to={`/products/${productId}/update`}><EditIcon />Update</Button>
								}
							</Fragment>
							:

								<Button variant="outline-success" onClick={(e) => login(e)}><ShoppingCartIcon />Add to Cart</Button>
						}


					</Card.Body>	
						
			
			</div>
			<div sm={{ span: 6, offset: 6 }} md={{ span: 4, offset: 4 }}>
  <Card>
    <Card.Img variant="top" src="holder.js/100px160"  width={250} height={250} />
    <Card.Footer>
    <div>
      <small className="text-muted">Author: {author}</small>
      </div>
      <div>
      <small className="text-muted">Publish Date: {publishedOn}</small>
      </div>
    </Card.Footer>
  </Card>				

							
			
			</div>
			</Stack>
		</Row>
	)
}