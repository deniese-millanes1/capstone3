import { Grid } from "@mui/material";
import { Navigate } from "react-router-dom";
import UserContext from "../../UserContext";
import { useContext } from "react";
import OrderView from "../../components/OrderView";

export default function UserOrder() {
    const {user} = useContext(UserContext)

    return (
        (user.id !== null) ?
        <Grid container>
            {(!user.isAdmin) ?
                <>
                <Grid
                    item
                    xs={12}
                    container
                    padding={5}
                    spacing={2}
                >
                    <OrderView />
                </Grid>
                </>
            :
            <Navigate to ="/" />}
        </Grid>
        :
        <Navigate to ="/login" />
    )
}