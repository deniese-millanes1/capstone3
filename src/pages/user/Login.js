import {useState, useEffect, useContext} from "react";
import {Navigate, useNavigate} from "react-router-dom";
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Link from '@mui/material/Link';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import FaceIcon from '@mui/icons-material/Face';
import Typography from '@mui/material/Typography';
import {createTheme, ThemeProvider} from '@mui/material/styles';
import {green, lightGreen} from '@mui/material/colors';
import Swal from "sweetalert2";
import UserContext from "../../UserContext";


function Copyright(props) {
  return (
    <Typography variant="body2" color="text.secondary" align="center" {...props}>
      {'Copyright © '}
      <Link color="inherit" href="https://www.linkedin.com/in/deniesemillanes/">
        The Den
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const theme = createTheme({
  palette: {
    primary: {
      main: green[300],
    },
    secondary: {
      main: green[300],
    },
  },
});


export default function Login() {

  const {user, setUser} = useContext(UserContext);
  const navigate = useNavigate();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isActive, setIsActive] = useState(false);

  function userLogin(e) {
    e.preventDefault()

    fetch(`https://secure-castle-27864.herokuapp.com/users/login`, {
      method:"POST", 
      headers: {
        "Content-Type": "application/json"
      }, 
      body: JSON.stringify(
        {
          email: email,
          password: password
        }
      )
    })
    .then(res => res.json())
    .then(data => {
      //console.log(data)

      if(typeof data.userAccessToken !== "undefined" || typeof data.adminAccessToken !== "undefined") {
        localStorage.setItem("token", data.userAccessToken || data.adminAccessToken)
        retrieveUserDetails(data.userAccessToken || data.adminAccessToken)

        Swal.fire(
          {
            title: "Login Successful",
            icon: "success",
            position: "bottom-start",
            toast: true,
            showConfirmButton: false,
            timer: 2222
          }
        ) 
        navigate("/")
      } else if (data.notRegistered) {
        Swal.fire(
          {
            title: "Authentication Failed",
            icon: "error",
            text: "The email is not yet registered.",
            position: "bottom-start",
            toast: true,
            showConfirmButton: false,
            timer: 2222
          }
        )
      } else if (data.incorrectPassword) {
        Swal.fire(
          {
            title: "Authentication Failed",
            icon: "error",
            text: "The password is incorrect.",
            position: "bottom-start",
            toast: true,
            showConfirmButton: false,
            timer: 2222
          }
        )
      }
    })
    setEmail("");
    setPassword("");
  }

  useEffect(() => {
    if(email !== "" && password !== "") {
      setIsActive(true)
    } else {
      setIsActive(false)
    }
  }, [email, password]);

  const retrieveUserDetails = (token) => {
    fetch("https://secure-castle-27864.herokuapp.com/users/details", {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)
      console.log(user)
      setUser({
        id: data._id,
        isAdmin: data.isAdmin
      }) 
      if (data.isAdmin === true) {
        navigate ("/admin")
      }
    })
  }


  return (
    
    (user.id !== null) ?
      <Navigate to ="/" />
    :    
      <ThemeProvider theme={theme}>
        <Grid container component="main" sx={{ height: '100vh' }}>
          <CssBaseline />
          <Grid
            item
            xs={false}
            sm={4}
            md={7}
            sx={{
              backgroundImage: 'url(https://source.unsplash.com/RrhhzitYizg)',
              backgroundRepeat: 'no-repeat',
              backgroundColor: (t) =>
                t.palette.mode === 'light' ? t.palette.grey[50] : t.palette.grey[900],
              backgroundSize: 'cover',
              backgroundPosition: 'center',
            }}
          />
          <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
            <Box
              sx={{
                my: 8,
                mx: 4,
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
              }}
            >
              <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
                <FaceIcon />
              </Avatar>
              <Typography component="h1" variant="h5">
                Log In
              </Typography>
              <Box component="form" noValidate onSubmit={(e) => userLogin(e)} sx={{ mt: 1 }}>
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  id="email"
                  label="Email Address"
                  name="email"
                  autoComplete="email"
                  autoFocus
                  value={email}
                  onChange={e => setEmail(e.target.value)}
                />
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  name="password"
                  label="Password"
                  type="password"
                  id="password"
                  autoComplete="current-password"
                   value={password}
                  onChange={e => setPassword(e.target.value)}
                />
                {
                  isActive ?                
                    <Button
                      type="submit"
                      fullWidth
                      variant="contained"
                      sx={{ mt: 3, mb: 2, bgcolor: 'secondary.main' }}
                    >
                      Sign In
                    </Button>
                  : 
                    <Button
                      type="submit"
                      fullWidth
                      variant="contained"
                      sx={{ mt: 3, mb: 2, bgcolor: 'secondary.main' }} disabled
                    >
                      Sign In
                    </Button>
                }
                <Grid container>
                  <Grid item xs>
                    <Link href="#" variant="body2" sx={{ color: 'secondary.main' }}>
                      Forgot password?
                    </Link>
                  </Grid>
                  <Grid item>
                    <Link href="#" variant="body2" sx={{ color: 'secondary.main' }}>
                      {"Don't have an account? Sign Up"}
                    </Link>
                  </Grid>
                </Grid>
                <Copyright sx={{ mt: 5 }} />
              </Box>
            </Box>
          </Grid>
        </Grid>
      </ThemeProvider>
  );
}