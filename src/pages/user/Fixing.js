import Banner from "../../components/Banner";


export default function Fixing() {
	
	const data = {
		title: "Service Temporary Unavailable",
		text: `This page is temporarily unable to service your request due to maintenance downtime or capacity problem. Please try again later. Our engineers are currently working on it`,
		url: "/",
		btnText: "Back Home"
	}
	
	return (				
		<Banner bannerProp={data} />
	)
}
