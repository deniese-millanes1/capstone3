import { useState, useEffect } from "react";
import { Navigate } from "react-router-dom";
import { Grid } from "@mui/material";
import UserContext from "../../UserContext";
import { useContext } from "react";
import CheckoutView from "../../components/CheckoutView";
 
export default function Checkout() {

    const {user} = useContext(UserContext)

    const [isCartEmpty, setIsCartEmpty] = useState(false)

    useEffect(() => {
        fetch(`https://secure-castle-27864.herokuapp.com/users/cart`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            if (data.length > 0) {
                setIsCartEmpty(false)
            } else {
                setIsCartEmpty(true)
            }
        })
    });

    return (
        (user.id !== null) ?
        <Grid container justifyContent="center">
            {(!user.isAdmin) ?
               <>
               {
                   isCartEmpty ?
                   <Navigate to ="/products" />
                   :
                   <CheckoutView />
               }
               </>
            :
            <Navigate to ="/" />}
        </Grid>
        :
        <Navigate to ="/login" />
    )
}