import {Fragment} from 'react';
import {Carousel, Container} from "react-bootstrap";
import Products from "./Products"


export default function Home() {
  return (  
    <Fragment>
      <Container>
        <Carousel fade >

          <Carousel.Item>
            <img width={500} height={250} alt="500x250"
            className="d-block w-100"
            src="https://source.unsplash.com/zMRLZh40kms"
            alt="First slide"
            />
            <Carousel.Caption>
              <h3>First slide label</h3>
              <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
            </Carousel.Caption>
          </Carousel.Item>

          <Carousel.Item>
            <img width={500} height={250} alt="500x250"
            className="d-block w-100"
            src="https://source.unsplash.com/jLwVAUtLOAQ"
            alt="Second slide"
            />
            <Carousel.Caption>
              <h3>Second slide label</h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
            </Carousel.Caption>
          </Carousel.Item>

          <Carousel.Item>
            <img width={500} height={250} alt="500x250"
            className="d-block w-100"
            src="https://source.unsplash.com/xY55bL5mZAM"
            alt="Third slide"
            />
            <Carousel.Caption>
              <h3>Third slide label</h3>
              <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
            </Carousel.Caption>
          </Carousel.Item>

        </Carousel>        
      </Container>

    <Products />
    </Fragment>
  )
}