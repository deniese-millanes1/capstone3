import { Navigate } from "react-router-dom"
import UserContext from "../../UserContext"
import { useContext } from "react"
import { Grid } from "@mui/material"
import CartProductView from "../../components/CartProductView"

export default function CartPage() {
    const {user} = useContext(UserContext)

    return (
        (user.id !== null) ?
        <Grid container justifyContent="center">
            {(!user.isAdmin) ?
                <CartProductView />
            :
            <Navigate to ="/" />}
        </Grid>
        :
        <Navigate to ="/login" />
    )
}