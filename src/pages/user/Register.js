import {useState,useEffect, useContext} from "react";
import {useNavigate} from "react-router-dom";
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import CreateIcon from '@mui/icons-material/Create';
import Typography from '@mui/material/Typography';
import {green} from '@mui/material/colors';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Swal from "sweetalert2";
import UserContext from "../../UserContext";


function Copyright(props) {
  return (
    <Typography variant="body2" color="text.secondary" align="center" {...props}>
      {'Copyright © '}
      <Link color="inherit" href="https://www.linkedin.com/in/deniesemillanes/">
        The Den
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const theme = createTheme({
  palette: {
    primary: {
      main: green[300],
    },
    secondary: {
      main: green[300],
    },
  },
});

export default function Register() {

  const {user, setUser} = useContext(UserContext)
  const navigate = useNavigate();
  const [firstName, setFirstName] = useState('')
  const [lastName, setLastName] = useState('')
  const [email, setEmail] = useState('');
  const [password1, setPassword1] = useState('');
  const [password2, setPassword2] = useState('');
  const [isActive, setIsActive] = useState(false);


  function registerUser(e) {
    e.preventDefault()

    fetch(`https://secure-castle-27864.herokuapp.com/users/register`, {
      method:"POST", 
      headers: {
        "Content-Type": "application/json"
      }, 
      body: JSON.stringify(
        {
          firstName: firstName,
          lastName: lastName,
          email: email,
          password: password1
        }
      )
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)

      if(data.emailAlreadyUsed) {
        Swal.fire(
          {
            title: "Email is already used",
            icon: "error",
            text: "The email is not yet registered.",
            position: "bottom-start",
            toast: true,
            showConfirmButton: false,
            timer: 2222
          }
        )       
        setFirstName("");
        setLastName("");
        setEmail("");
        setPassword1("");
        setPassword2(""); 
      } else if (data.registerError) {
        Swal.fire(
          {
            title: "Register Error",
            icon: "error",
            text: "The email is not yet registered.",
            position: "bottom-start",
            toast: true,
            showConfirmButton: false,
            timer: 2222
          } 
          )     
      } else if (data.registerSuccess) {
        Swal.fire(
          {
            title: "Register Successful",
            icon: "success",
            text: "Welcome to The Den",
            position: "bottom-start",
            toast: true,
            showConfirmButton: false,
            timer: 2222
          }
        )  
        navigate("/login")
      }   
      // } else if (password1 !== password2) {
      //   Swal.fire(
      //     {
      //       title: "Error",
      //       icon: "error",
      //       text: "The password are not identical",
      //       position: "bottom-start",
      //       toast: true,
      //       showConfirmButton: false,
      //       timer: 2222
      //     } 
      //     )        
      // }
    })

  }

  useEffect(() => {
    if((firstName !== "" && lastName !== "" && email !== "" && password1 !== "" && password2 !== "") && (password1 === password2)) {
      setIsActive(true);
      console.log(user)
    } else {
      setIsActive(false)
    }
  }, [firstName, lastName, email, password1, password2])


  return (
 
      <ThemeProvider theme={theme}>
        <Grid container component="main" sx={{ height: '100vh' }}>
          <CssBaseline />
          <Grid
            item
            xs={false}
            sm={4}
            md={7}
            sx={{
              backgroundImage: 'url(https://source.unsplash.com/jLwVAUtLOAQ)',
              backgroundRepeat: 'no-repeat',
              backgroundColor: (t) =>
                t.palette.mode === 'light' ? t.palette.grey[50] : t.palette.grey[900],
              backgroundSize: 'cover',
              backgroundPosition: 'center',
            }}
          />
          <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
            <Box
              sx={{
                my: 8,
                mx: 4,
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
              }}
            >
              <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
                <CreateIcon />
              </Avatar>
              <Typography component="h1" variant="h5">
                Register
              </Typography>
              <Box component="form" noValidate onSubmit = {(e) => registerUser(e)} sx={{ mt: 1 }}>
              <Grid container spacing={2}>
                <Grid item xs={12} sm={6}>
                  <TextField
                    autoComplete="given-name"
                    name="firstName"
                    required
                    fullWidth
                    id="firstName"
                    label="First Name"
                    autoFocus
                    value={firstName}
                    onChange={e => setFirstName(e.target.value)} 
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  <TextField
                    required
                    fullWidth
                    id="lastName"
                    label="Last Name"
                    name="lastName"
                    autoComplete="family-name"
                    value={lastName}
                    onChange={e => setLastName(e.target.value)} 
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    required
                    fullWidth
                    id="email"
                    label="Email Address"
                    name="email"
                    autoComplete="email"
                    value={email}
                    onChange={e => setEmail(e.target.value)}                   
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    required
                    fullWidth
                    name="password1"
                    label="Password"
                    type="password"
                    id="password1"
                    value={password1}
                    onChange={e => setPassword1(e.target.value)} 
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    required
                    fullWidth
                    name="password2"
                    label="Verify Password"
                    type="password"
                    id="password2"
                    value={password2}
                    onChange={e => setPassword2(e.target.value)} 
                  />
                </Grid>
              </Grid>

            {
              isActive ?
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  sx={{ mt: 3, mb: 2 }}
                >
                  Register
                </Button>
              :
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  sx={{ mt: 3, mb: 2 }} disabled
                >
                  Register
                </Button>
            }

              <Grid container justifyContent="flex-end">
                <Grid item>
                  <Link href="#" variant="body2">
                    Already have an account? Sign in
                  </Link>
                </Grid>
              </Grid>

                
                <Copyright sx={{ mt: 5 }} />
              </Box>
            </Box>
          </Grid>
        </Grid>
      </ThemeProvider>

  );
}