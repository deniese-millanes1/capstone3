import Banner from "../../components/Banner";


export default function NotFound() {
	
	const data = {
		title: "404 - Not Found",
		url: "/",
		btnText: "Back Home"
	}
	
	return (
		<Banner bannerProp={data} />	
	)
}
