import PropTypes from "prop-types";
import {Fragment, useEffect, useState} from "react";
import ProductView from "../../components/ProductView";


export default function Products() {

	const [products, setProducts] = useState([]);

	useEffect(() => {
		fetch("https://secure-castle-27864.herokuapp.com/products")
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setProducts(data.map(result => {
				return (
					<ProductView key={result._id} productViewProp={result} />)
			}))

		})
	}, [])


	return (
		<Fragment>
			{products}
		</Fragment>
	)
}


ProductView.propTypes = {
	courseProp: PropTypes.shape( 
		{
			name: PropTypes.string.isRequired,
			description: PropTypes.string.isRequired,
			price: PropTypes.number.isRequired,
			author: PropTypes.string.isRequired,
			publishedOn: PropTypes.string.isRequired
		}
	)
}