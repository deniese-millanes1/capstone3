import {useState, useEffect, useContext, Fragment} from 'react';
import {useParams, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import {Stack, Row, Col, Card, Button, Form} from "react-bootstrap";
import CloseIcon from '@mui/icons-material/Close';
import ArrowLeftIcon from '@mui/icons-material/ArrowLeft';
import CheckIcon from '@mui/icons-material/Check';

export default function UpdateProduct() {

	const {productId} = useParams();
	const navigate = useNavigate();
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [author, setAuthor] = useState("");
	const [publishedOn, setPublishedOn] = useState("");
	const [isActive, setIsActive] = useState(true);
	const [price, setPrice] = useState(0);
	const [stocks, setStocks] = useState('');
	const [imageURL, setImageURL] = useState('');


	function updateProduct(e) {
	e.preventDefault()
    fetch(`https://secure-castle-27864.herokuapp.com/products/${productId}`, {
      method:"PUT", 
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }, 
      body: JSON.stringify(
        {
          name: name,
          description: description,
          author: author,
          publishedOn: publishedOn,
          price: price,
          stocks: stocks,
          imageURL: imageURL
        }
      )
    })
    .then(data => {
      console.log(data)

      if(data === null) {
        Swal.fire(
          {
            title: "Deactivating Error",
            icon: "error",
            position: "bottom-start",
            toast: true,
            showConfirmButton: false,
            timer: 2222
          }
        )       
      } else if (data !== null) {
        Swal.fire(
          {
            title: "Product Updated",
            icon: "success",
            position: "bottom-start",
            toast: true,
            showConfirmButton: false,
            timer: 2222
          } 
          )
        navigate("/products/all")      
      } 

    })
  }

	useEffect(() => {

		//console.log(productId);
		fetch(`https://secure-castle-27864.herokuapp.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setName(data.name);
			setDescription(data.description)
			setPrice(data.price);
			setAuthor(data.author);
			setPublishedOn(data.publishedOn);
			setStocks(data.stocks);
			setImageURL(data.imageURL);
			

		})
	},[productId])

	return (
			<Form className="mt-3" onSubmit = {(e) => updateProduct(e)}>

			<Form.Group>
				<Form.Label>name</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter first name"
					value={name}
					onChange={e => setName(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>author</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter last name"
					value={author}
					onChange={e => setAuthor(e.target.value)}
					required
				/>
			</Form.Group>


			<Form.Group>
				<Form.Label>publishedOn</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter mobile number"
					value={publishedOn}
					onChange={e => setPublishedOn(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>description</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter mobile number"
					value={description}
					onChange={e => setDescription(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group className="mb-3" controlId="password1">
				<Form.Label>price</Form.Label>
				<Form.Control 
					type="number" 
					placeholder="Password"
					value={price}
					onChange= {e => setPrice(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group className="mb-3" controlId="">
				<Form.Label>stocks</Form.Label>
				<Form.Control 
					type="number" 
					placeholder="Password"
					value={stocks}
					onChange= {e => setStocks(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group className="mb-3" controlId="">
				<Form.Label>imageURL</Form.Label>
				<Form.Control 
					type="url" 
					value={imageURL}
					onChange= {e => setImageURL(e.target.value)}
					required
				/>
			</Form.Group>			
<Button variant="primary" type="submit" id="submitBtn">
			  		  Update
			  		</Button>			
			 </Form>

	)
}