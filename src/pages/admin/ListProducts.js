import PropTypes from "prop-types";
import {Fragment, useEffect, useState, useContext} from "react";
import AdminProductView from "../../components/AdminProductView"
import UserContext from "../../UserContext";


export default function ListProducts() {
	const [products, setProducts] = useState([]);
	const {user, setUser} = useContext(UserContext);

//console.log(products);

	useEffect(() => {
		fetch("https://secure-castle-27864.herokuapp.com/products/all", {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
		.then(res => res.json())
		.then(data => {
//			console.log(data)
			
			setProducts(data.map(result => {
				// console.log(result)
				return (
					
					<AdminProductView key={result._id} adminProductViewProp={result} />)
			}))

		})
	}, [])


	return (
		<Fragment>
			{products}
		</Fragment>
	)
}

AdminProductView.propTypes = {
	courseProp: PropTypes.shape( 
		{
			name: PropTypes.string.isRequired,
			description: PropTypes.string.isRequired,
			price: PropTypes.number.isRequired,
			author: PropTypes.string.isRequired,
			publishedOn: PropTypes.string.isRequired
		}
	)
}
