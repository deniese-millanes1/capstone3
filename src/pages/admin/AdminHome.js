import {Button} from "react-bootstrap";
import {Link} from "react-router-dom";


export default function AdminHome() {

	return (	
		<div className="d-grid gap-2">
		<h1>Welcome Back, Admin</h1>
		  <Button variant="outline-primary" size="lg" as={Link} to="/new-product">>
		    Create new product
		  </Button>			
		  <Button variant="outline-success" size="lg" as={Link} to="/products/all">>
		   Retrieve list of all products (available or unavailable)
		  </Button>	    
		</div>		
	)
}