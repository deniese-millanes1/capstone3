import {useState, useEffect, useContext} from "react";
import {Navigate, useNavigate} from "react-router-dom";
import { Form, Button } from 'react-bootstrap';
import Swal from "sweetalert2";


export default function NewProduct() {

	const [name, setName] = useState('')
	const [author, setAuthor] = useState('')
	const [publishedOn, setPublishedOn] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [stocks, setStocks] = useState('');
	const [imageURL, setImageURL] = useState('');
	const navigate = useNavigate();

  function createNewProduct(e) {
    e.preventDefault()

    fetch(`https://secure-castle-27864.herokuapp.com/products`, {
      method:"POST", 
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }, 
      body: JSON.stringify(
        {
          name: name,
          author: author,
          publishedOn: publishedOn,
          description: description,
          price: price,
          stocks: stocks,
          imageURL: imageURL

        }
      )
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)

      if(data.productAlreadyCreated) {
        Swal.fire(
          {
            title: "Product is already created",
            icon: "error",
            text: "The product you are adding is already in the database",
            position: "bottom-start",
            toast: true,
            showConfirmButton: false,
            timer: 2222
          }
        )       
		setName("");
		setAuthor("");
		setPublishedOn("");
		setDescription("");
		setPrice("");
      } else if (data.creatingProductError) {
        Swal.fire(
          {
            title: "Creating Product Error",
            icon: "error",
            text: "There is an error in saving/creating a product",
            position: "bottom-start",
            toast: true,
            showConfirmButton: false,
            timer: 2222
          } 
          )     
      } else {
        Swal.fire(
          {
            title: "Create Successful",
            icon: "success",
            position: "bottom-start",
            toast: true,
            showConfirmButton: false,
            timer: 2222
          }
        )  
        navigate("/admin")
      }   
      // } else if (password1 !== password2) {
      //   Swal.fire(
      //     {
      //       title: "Error",
      //       icon: "error",
      //       text: "The password are not identical",
      //       position: "bottom-start",
      //       toast: true,
      //       showConfirmButton: false,
      //       timer: 2222
      //     } 
      //     )        
      // }
    })

  }
	return (
			<Form className="mt-3" onSubmit = {(e) => createNewProduct(e)}>

			<Form.Group>
				<Form.Label>name</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter first name"
					value={name}
					onChange={e => setName(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>author</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter last name"
					value={author}
					onChange={e => setAuthor(e.target.value)}
					required
				/>
			</Form.Group>


			<Form.Group>
				<Form.Label>publishedOn</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter mobile number"
					value={publishedOn}
					onChange={e => setPublishedOn(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>description</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter mobile number"
					value={description}
					onChange={e => setDescription(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group className="mb-3" controlId="">
				<Form.Label>price</Form.Label>
				<Form.Control 
					type="number" 
					placeholder="Password"
					value={price}
					onChange= {e => setPrice(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group className="mb-3" controlId="">
				<Form.Label>stocks</Form.Label>
				<Form.Control 
					type="number" 
					placeholder="Password"
					value={stocks}
					onChange= {e => setStocks(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group className="mb-3" controlId="">
				<Form.Label>imageURL</Form.Label>
				<Form.Control 
					type="url" 
					value={imageURL}
					onChange= {e => setImageURL(e.target.value)}
					required
				/>
			</Form.Group>
<Button variant="primary" type="submit" id="submitBtn">
			  		  Create
			  		</Button>			
			 </Form>
	)
}