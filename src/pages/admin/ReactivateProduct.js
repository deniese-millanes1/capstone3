import {useState, useEffect, useContext, Fragment} from 'react';
import {useParams, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import {Stack, Row, Col, Card, Button} from "react-bootstrap";
import CloseIcon from '@mui/icons-material/Close';
import ArrowLeftIcon from '@mui/icons-material/ArrowLeft';
import CheckIcon from '@mui/icons-material/Check';

export default function ReactivateProduct() {

	const {productId} = useParams();
	const navigate = useNavigate();
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [author, setAuthor] = useState("");
	const [publishedOn, setPublishedOn] = useState("");
	const [isActive, setIsActive] = useState(true);
	const [price, setPrice] = useState(0);

	function reactivateProduct(e) {
	e.preventDefault()
    fetch(`https://secure-castle-27864.herokuapp.com/products/${productId}/unarchive`, {
      method:"PUT", 
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(data => {
      console.log(data)

      if(data === null) {
        Swal.fire(
          {
            title: "Deactivating Error",
            icon: "error",
            position: "bottom-start",
            toast: true,
            showConfirmButton: false,
            timer: 2222
          }
        )       
      } else if (data !== null) {
        Swal.fire(
          {
            title: "Product Reactivated",
            icon: "success",
            position: "bottom-start",
            toast: true,
            showConfirmButton: false,
            timer: 2222
          } 
          )
        navigate("/products/all")      
      } 
    })
  }

	useEffect(() => {

		//console.log(productId);
		fetch(`https://secure-castle-27864.herokuapp.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setName(data.name);
			setDescription(data.description)
			setPrice(data.price);
			setAuthor(data.author);
			setPublishedOn(data.publishedOn);
			setIsActive(data.isActive);

		})
	},[productId])

  useEffect(() => {
    if(isActive) {
      setIsActive(true)
    } else {
      setIsActive(false)
    }
  }, [isActive]);
	return (
		<Fragment>
		<h1>Deactivate Product</h1>
		<Button className="text-center ml-3 mr-3" variant="primary" as={Link} to={`/products/all`} sx={{ mt: 1 }}><ArrowLeftIcon />Go Back</Button>{' '}

		<Row >
		<Card.Header as="h5" className="text-center mt-3 mb-3">{name}</Card.Header>
		<Stack direction="horizontal" gap={3}  className="mt-7 mb-3">
				<div sm={6} md={6}>
			
				
					<Card.Body>
						<Card.Title></Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>

							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>
						
						
						{ isActive ?
								<Fragment>
									<Card.Subtitle>Stock Availability: </Card.Subtitle>
									<Card.Text>Active</Card.Text>
									<Button className="text-center ml-3 mr-3" variant="danger" as={Link} to={`/products/${productId}/archive`} sx={{ mt: 1 }}  sx={{ mt: 1 }}><CloseIcon />Deactivate</Button>{' '}
								</Fragment>
							:
								<Fragment>
									<Card.Subtitle>Stock Availability: </Card.Subtitle>
									<Card.Text>Not Active</Card.Text>
									{/*onSubmit = {(e) => reactivateProduct(e)} sx={{ mt: 1 }}*/}
									<Button className="text-center mt-3 mb-3" variant="success" as={Link} to={`/products/${productId}/archive` } onClick = {(e) => reactivateProduct(e) }><CheckIcon />Reactivate</Button>{' '}
								</Fragment>
						}
					</Card.Body>

			</div>
			<div sm={{ span: 6, offset: 6 }} md={{ span: 4, offset: 4 }}>
  <Card>
    <Card.Img variant="top" src="holder.js/100px160"  width={250} height={250} />
    <Card.Footer>
    <div>
      <small className="text-muted">Author: {author}</small>
      </div>
      <div>
      <small className="text-muted">Publish Date: {publishedOn}</small>
      </div>
    </Card.Footer>
  </Card>				

							
			
			</div>

			</Stack>

		</Row>			
		</Fragment>

	)
}