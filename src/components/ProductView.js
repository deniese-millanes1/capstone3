import {Fragment, useState, useContext} from "react";
import {Stack, Row, Card, Button} from "react-bootstrap";
import {Link} from 'react-router-dom';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import EditIcon from '@mui/icons-material/Edit';
import UserContext from "../UserContext";


export default function ProductView({ productViewProp }) {

    const { name, isActive, price, author, publishedOn, _id, imageURL, stocks } = productViewProp;
    console.log(productViewProp)
    const { user } = useContext(UserContext);
    console.log(isActive)

    const [readMore, setReadMore] = useState(false);
    const descriptionText = productViewProp.description;
    const linkName = readMore ? 'Read Less << ' : 'Read More >> '

    return (

        <Row >
		<Card.Header as="h5" className="text-center mt-3 mb-3">{name}</Card.Header>
		<Stack direction="horizontal" gap={3}  className="mt-7 mb-3">
				<div sm={6} md={6}>
			
				
					<Card.Body>

							<Card.Subtitle ><h3 >Description:</h3></Card.Subtitle>    
{/*					      <a className="read-more-link" onClick={()=>{setReadMore(!readMore)}}><h6>{linkName}</h6></a>
					      {readMore && descriptionText}*/}
					     	<h5 className="text1">
					      {
					      	readMore ?
					      		descriptionText
					      	:
					      		`${descriptionText.substring(0,250)}`
					      }
					      {
						      (descriptionText.length > 250) ?
						      <Button variant="link"size="sm" onClick={() => setReadMore(!readMore)}>{linkName}</Button>
						    :
						    	descriptionText
						  	}	
								</h5>
						{' '}<Card.Subtitle><h3>Price:</h3></Card.Subtitle>
							<Card.Text><h5 className="text1">PhP {price}</h5></Card.Text>
						
					{
						(user.isAdmin === true) ?
							<Fragment>
								<Card.Subtitle><h3>Stocks:</h3></Card.Subtitle>
								<Card.Text><h5 className="text1">{stocks}</h5></Card.Text>
								<Button variant="primary" as={Link} to={`/products/${_id}/update`}><EditIcon />Update</Button>	
							</Fragment>
						:
							<Button variant="outline-success" as={Link} to={`/products/${_id}`}><ShoppingCartIcon />Add To Cart</Button>
					}	
					
					</Card.Body>	
						
			
			</div>
			<div sm={{ span: 6, offset: 6 }} md={{ span: 4, offset: 4 }}>
			
  <Card className="d-none d-md-block">
    <Card.Img variant="top" src={imageURL}  style={{width: "18rem"}} />
    <Card.Footer>
    <div>
      <small className="text-muted text1">Author: {author}</small>
      </div>
      <div>
      <small className="text-muted text1">Publish Date: {publishedOn}</small>
      </div>
    </Card.Footer>
  </Card>				

							
			
			</div>
			</Stack>
		</Row>

    )
}