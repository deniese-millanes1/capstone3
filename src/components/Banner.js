import {Row,Col,Button} from "react-bootstrap";
import {Link} from "react-router-dom";


export default function Banner({bannerProp}) {

	const {title, text, url, btnText} = bannerProp

	return (
		<Row>
			<Col className="p-5 text-center">			
				<h1>{title}</h1>
				<p>{text}</p>
				<Button variant="outline-success" as={Link} to={url}>{btnText}</Button>
			</Col>
		</Row>
	)
}