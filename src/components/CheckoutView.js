import { Grid } from "@mui/material";
import { Typography } from "@mui/material";
import { Box, Button } from "@mui/material";
import { useState } from "react";
import { TextField } from "@mui/material";
import { FormControl, Radio, FormControlLabel, FormLabel, RadioGroup } from "@mui/material";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";

export default function CheckoutView() {
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [address, setAddress] = useState('');
    const [payment, setPayment] = useState('');

    const navigate = useNavigate();

    function checkOutOrder(e) {
        e.preventDefault()
        
        function isNotEmpty(input) {
            return (input !== '') ? true : false;
        }
    
        if (isNotEmpty(firstName) && 
            isNotEmpty(lastName) &&
            isNotEmpty(mobileNo) &&
            (mobileNo.length === 11)){
                checkOutMyOrder()
        } else {
            Swal.fire({
                title: "Missing inputs!",
                icon: "error", 
                text: "Please fill in all inputs."
            })
            }

        function checkOutMyOrder() {
            Swal.fire({
                title: 'Are you sure?',
                text: "Order will be placed after this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#228B22',
                confirmButtonText: 'Yes, checkout!'
                }).then((result) => {
                if (result.isConfirmed) {
                    fetch(`https://secure-castle-27864.herokuapp.com/users/checkout`, {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                            Authorization: `Bearer ${localStorage.getItem('token')}`
                        }
                    })
                    .then(res => res.json())
                    .then(data => {
                        
                        if (data) {
                            setFirstName('')
                            setLastName('')
                            setMobileNo('')
                            setAddress('')
                            setPayment('')
            
                            Swal.fire({
                                title: 'Order Sucess!',
                                text: "You have successfully ordered your items!",
                                icon: 'success',
                                showCancelButton: true,
                                confirmButtonColor: '#228B22',
                                confirmButtonText: 'See my orders',
                                cancelButtonText: 'Go back to products'
                                }).then((result) => {
                                if (result.isConfirmed) {
                                    navigate("/orders")
                                } else {
                                    navigate("/products")
                                }
                            })
        
                        } else {
                            Swal.fire({
                                title: "Something went wrong!",
                                icon: "error", 
                                text: "Please try again."
                            })
                        }
                    })
                }
            })
        }
    }

    return (
        <Grid container justifyContent="center" >
            <Grid item xs={12} p={3} textAlign="center">
                <h3><b>Delivery Details</b></h3>
            </Grid>

            <Grid item xs={12} sm={6} md={5} px={3} pb={3}>
                <Box
                    component="form"
                    onSubmit = {(e) => checkOutOrder(e)}
                >
                    <Grid container>
                        <TextField
                            sx={{
                                marginBottom: 2
                            }}
                            required
                            id="reg-firstname"
                            label="First Name"
                            type="text"
                            autoFocus
                            value = {firstName}
                            onChange = {e => setFirstName(e.target.value)}
                            fullWidth
                        />
                        <TextField
                            sx={{
                                marginBottom: 2
                            }}
                            required
                            label="Last Name"
                            type="text"
                            value = {lastName}
                            onChange = {e => setLastName(e.target.value)}
                            fullWidth
                        />
                        <TextField
                            sx={{
                                marginBottom: 2
                            }}
                            required
                            label="Mobile Number"
                            type="text"
                            value = {mobileNo}
                            onChange = {e => setMobileNo(e.target.value)}
                            fullWidth
                        />
                        <TextField
                            sx={{
                                marginBottom: 2
                            }}
                            required
                            label="Address"
                            type="address"
                            autoComplete="address"
                            value = {address}
                            onChange = {e => setAddress(e.target.value)}
                            fullWidth
                        />
                        <FormControl>
                            <FormLabel id="demo-radio-buttons-group-label">How would you like to pay?</FormLabel>
                            <RadioGroup
                                aria-labelledby="demo-radio-buttons-group-label"
                                value={payment}
                                onChange={(e) => setPayment(e.target.value)}
                                name="radio-buttons-group"
                            >
                                <FormControlLabel value="cod" control={<Radio />} label="COD" />
                                <FormControlLabel value="debit" control={<Radio />} label="Debit" />
                            </RadioGroup>
                        </FormControl>

                        <Button
                            id="reg-button"
                            type="submit"
                            variant="contained"
                            disableElevation
                            sx={{
                                color: "#f1f1f1",
                                backgroundColor: "#000",
                                borderRadius: 6,
                                padding: 1,
                                paddingX: 6,
                                mt: 2,
                            }}
                            fullWidth
                        >
                            Place Order
                        </Button>
                    </Grid>
                </Box>
            </Grid>
        </Grid>
    )
}