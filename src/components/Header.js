import {useState, Fragment,useContext} from "react";
import {Navbar, Nav, Stack, Container, Button, Form, FormControl,Row, Col} from "react-bootstrap";
import {Link} from "react-router-dom";
import AdminPanelSettingsIcon from '@mui/icons-material/AdminPanelSettings';
import FaceIcon from '@mui/icons-material/Face';
import CreateIcon from '@mui/icons-material/Create';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import HomeIcon from '@mui/icons-material/Home';
import MenuBookIcon from '@mui/icons-material/MenuBook';
import StarBorderIcon from '@mui/icons-material/StarBorder';
import UserContext from "../UserContext";


export default function Header() {

  const {user} = useContext(UserContext);

	return (
    <>
      <Navbar expand="lg">
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav" className="justify-content-end">
          {
            (user.isAdmin === true) ?
            <Nav>
            {
              (user.id !== null) ?
                <Fragment>
                  <Navbar.Brand className="button" as = {Link} to="/admin"><AdminPanelSettingsIcon fontSize="large"/>Admin</Navbar.Brand>
                  <Navbar.Brand className="button" as = {Link} to="/logout"><FaceIcon fontSize="large"/>Log Out</Navbar.Brand>
                </Fragment> 
              :
                <Fragment>
                  <Navbar.Brand className="button" as = {Link} to="/login"><FaceIcon fontSize="large"/>Log In</Navbar.Brand>
                  <Navbar.Brand className="button" as = {Link} to="/register"><CreateIcon fontSize="large"/>Register</Navbar.Brand>
                </Fragment> 
            }
              <Form className="d-flex">
                <FormControl
                  type="search"
                  placeholder="Search"
                  className="me-2"
                  aria-label="Search"
                />
                <Button variant="outline-success">Search</Button>
              </Form>        
            </Nav>
          :
            <Nav>
            {
              (user.id !== null) ?
                <Fragment>
                  <Navbar.Brand className="button" as = {Link} to="/cart"><ShoppingCartIcon fontSize="large"/>Cart</Navbar.Brand>
                  <Navbar.Brand className="button" as = {Link} to="/orders">Order</Navbar.Brand>
                  <Navbar.Brand className="button" as = {Link} to="/logout"><FaceIcon fontSize="large"/>Log Out</Navbar.Brand>
                </Fragment> 
              :
                <Fragment>
                  <Navbar.Brand className="button" as = {Link} to="/login"><FaceIcon fontSize="large"/>Log In</Navbar.Brand>
                  <Navbar.Brand className="button" as = {Link} to="/register"><CreateIcon fontSize="large"/>Register</Navbar.Brand>
                </Fragment> 
            }
              <Form className="d-flex">
                <FormControl
                  type="search"
                  placeholder="Search"
                  className="me-2"
                  aria-label="Search"
                />
                <Button variant="outline-success">Search</Button>
              </Form>        
            </Nav>            
          }

        </Navbar.Collapse>  
      </Navbar>

      <Stack>
        <div className="header ml-3" as = {Link} to="/">
          <MenuBookIcon fontSize="x-large"/>
          The Den
        </div>   
      </Stack>

      <Navbar className="mint justify-content-center">
          <Nav className="d-none d-md-flex justify-content-center">
            <Navbar.Brand className="headerLink p-3" as = {Link} to="/">Home</Navbar.Brand>
            <Navbar.Brand className="headerLink p-3" as = {Link} to="/products">All Available Products</Navbar.Brand>
            <Navbar.Brand className="headerLink p-3" as = {Link} to="/books/best-seller">Best Sellers</Navbar.Brand>
            <Navbar.Brand className="headerLink p-3" as = {Link} to="/books/new-releases">New Releases</Navbar.Brand>
            <Navbar.Brand className="headerLink p-3" as = {Link} to="/genre">Shop By Genres</Navbar.Brand>
            <Navbar.Brand className="headerLink p-3" as = {Link} to="/books/exclusive-online-deals"><StarBorderIcon fontSize="large"/>Deals</Navbar.Brand>
          </Nav>  
      </Navbar>
    </>
  )
}