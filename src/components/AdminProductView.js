import {Fragment, useState, useEffect} from "react";
import {Stack, Row, Col, Card, Button} from "react-bootstrap";
import { Link } from 'react-router-dom';
import CloseIcon from '@mui/icons-material/Close';
import CheckIcon from '@mui/icons-material/Check';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import EditIcon from '@mui/icons-material/Edit';
import Swal from "sweetalert2";


export default function AdminProductView({adminProductViewProp}) {

	const {name, description, isActive, price, author, publishedOn, _id, imageURL, stocks} = adminProductViewProp;

	return (

		<Row >
		<Card.Header as="h5" className="text-center mt-3 mb-3">{name}</Card.Header>
		<Stack direction="horizontal" gap={3}  className="mt-7 mb-3">
				<div sm={6} md={6}>
			
				
					<Card.Body>
						<Card.Title></Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>

							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>
						{ (isActive) ?
								<Fragment>
									<Card.Subtitle>Stock Availability: </Card.Subtitle>
									<Card.Text>{stocks}</Card.Text>
									<Card.Text>Active</Card.Text>
									<Button className="text-center ml-3 mr-3" variant="danger" as={Link} to={`/products/${_id}/archive`} sx={{ mt: 1 }}><CloseIcon />Deactivate</Button>{' '}
								</Fragment>
							:
								<Fragment>
									<Card.Subtitle>Stock Availability: </Card.Subtitle>
									<Card.Text>{stocks}</Card.Text>
									<Card.Text>Inactive</Card.Text>
									{/*onSubmit = {(e) => reactivateProduct(e)} sx={{ mt: 1 }}*/}
									<Button className="text-center mt-3 mb-3" variant="success" as={Link} to={`/products/${_id}/unarchive` } ><CheckIcon />Reactivate</Button>{' '}
								</Fragment>
						}
						{/*onSubmit = {(e) => updateProduct(e)} sx={{ mt: 1 }}*/}
						<Button className="text-center mt-3 mb-3"variant="primary" as={Link} to={`/products/${_id}`}  sx={{ mt: 1 }}><EditIcon />Update</Button>{' '}
						
					</Card.Body>	
						
			
			</div>
			<div sm={{ span: 6, offset: 6 }} md={{ span: 4, offset: 4 }}>
  <Card>
    <Card.Img variant="top" src="holder.js/100px160"  width={250} height={250} />
    <Card.Footer>
    <div>
      <small className="text-muted">Author: {author}</small>
      </div>
      <div>
      <small className="text-muted">Publish Date: {publishedOn}</small>
      </div>
    </Card.Footer>
  </Card>				

							
			
			</div>
			</Stack>
		</Row>

	)
}
