import {useState, useEffect } from "react";
import {Link} from 'react-router-dom';
import { Grid } from "@mui/material";
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import { IconButton } from "@mui/material";
import {Box } from "@mui/material";
import EditIcon from '@mui/icons-material/Edit';
import Swal from "sweetalert2";
import { Button } from "@mui/material";

export default function CartProductView() {
    const [products, setProducts] = useState([]);
    const [total, setTotal] = useState();

    useEffect(() => {
        fetch(`https://secure-castle-27864.herokuapp.com/users/cart`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            setProducts(data)
        })
    }, [products, total]);
    
    function removeItem(product) {
        Swal.fire({
            title: 'Are you sure?',
            text: "Product will be deleted and cannot be retrieved!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#990f02',
            cancelButtonColor: '#4b5320',
            confirmButtonText: 'Yes, delete!'
            }).then((result) => {
            if (result.isConfirmed) {
                fetch(`https://secure-castle-27864.herokuapp.com/users/${product.productId}/cart`,{
                    method: 'DELETE',
                    headers: {
                        Authorization: `Bearer ${localStorage.getItem('token')}`
                    },
                })
                Swal.fire(
                'All set!',
                'Order deleted!',
                'success'
                )
            }
            }, [products])
    }

    async function editItem(product) {
        const { value: number } = await Swal.fire({
            input: 'number',
            inputLabel: 'Modify Quantity',
            inputPlaceholder: 'Enter the quantity'
          })
          
        if (number > 0) {
            Swal.fire({
                title: 'Are you sure?',
                text: "Quantity will be modified after this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#990f02',
                cancelButtonColor: '#4b5320',
                confirmButtonText: 'Yes, modify!'
                }).then((result) => {
                if (result.isConfirmed) {
                    fetch(`https://secure-castle-27864.herokuapp.com/${product.productId}/cart`,{
                        method: 'PUT',
                        headers: {
                            'Content-Type': 'application/json',
                            Authorization: `Bearer ${localStorage.getItem('token')}`
                        },
                        body: JSON.stringify({
                            quantity: number
                        })
                    })
                    .then(res => res.json())
                    .then(data => {
                        if (data) {
                            Swal.fire(
                                'All set!',
                                'Quantity modified!',
                                'success'
                            )
                        } else {
                            Swal.fire({
                                title: "Something went wrong!",
                                icon: "error", 
                                text: "Please try again."
                            })
                        }
                    })
                    
                }
            })
        } else {
            Swal.fire({
                title: "Cannot have 0 or less",
                icon: "error", 
                text: "Please try again."
            })
        }
    }

    useEffect(() => {
        fetch(`https://secure-castle-27864.herokuapp.com/users/total`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            if (data !== false) {
                setTotal(data.totalAmount)
            }
        })
    });

    // function emptyCart() {
    //     Swal.fire({
    //         title: 'Are you sure?',
    //         text: "Cart will be emptied!",
    //         icon: 'warning',
    //         showCancelButton: true,
    //         confirmButtonColor: '#990f02',
    //         cancelButtonColor: '#4b5320',
    //         confirmButtonText: 'Yes, empty!'
    //         }).then((result) => {
    //         if (result.isConfirmed) {
    //             fetch(`https://secure-castle-27864.herokuapp.com/users/cart`,{
    //                 method: 'DELETE',
    //                 headers: {
    //                     Authorization: `Bearer ${localStorage.getItem('token')}`
    //                 },
    //             })
    //             .then(res => res.json())
    //             .then(data => {
    //                 if (data) {
    //                     Swal.fire(
    //                         'All set!',
    //                         'Cart Emptied',
    //                         'success'
    //                     )
    //                 } else {
    //                     Swal.fire({
    //                         title: "Something went wrong!",
    //                         icon: "error", 
    //                         text: "Please try again."
    //                     })
    //                 }
    //             })
                
    //         }
    //     })
    // }

    return (
        <>
        {
            products.map((product) => {
                return (
                    <Grid item xs={12} borderBottom={1} py={5} borderColor="grey.500" key={product.productId}>
                        <Grid container justifyContent="center" alignItems="flex-start" spacing={2}>
                            <Grid item xs={12} sm={3} md={2}>
                                <IconButton
                                    sx={{ color: "#990f02"}}
                                    onClick= {() => removeItem(product)}
                                >
                                    <DeleteOutlineIcon />
                                </IconButton>
    
                                <IconButton
                                    sx={{ color: "#4b5320"}}
                                    onClick= {() => editItem(product)}
                                >
                                    <EditIcon />
                                    
                                </IconButton>
    
                                <h3>Quantity: {product.quantity}</h3>
                            </Grid>
    
                            <Grid item xs={12} sm={4} md={5}>
                                <img src={product.imageURL}  alt="img" style={{width: "100%"}}/>
                            </Grid>
                            
                            <Grid item xs={12} sm={5}>
                                <Grid container justifyContent="space-between">
                                    <Grid item xs={8} sm={8}>
                                        <h2 >{product.name}</h2>
                                        <h6>{product.description}</h6>
                                    </Grid>
    
                                    <Grid item xs={4} sm={3}>
                                        <h2 ><Box sx={{display: "inline", fontSize: "0.5em"}}>&#8369;</Box><strong>{product.price}</strong></h2>
                                    </Grid>
                                </Grid>
    
                                <Grid container justifyContent="space-between" mt={6}>
                                    <Grid item xs={8} sm={8}>
                                        <h2 >Subtotal</h2>
                                    </Grid>
    
                                    <Grid item xs={4} sm={3}>
                                        <h2 ><Box sx={{display: "inline", fontSize: "0.5em"}}>&#8369;</Box><strong>{product.price }</strong></h2>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                )
            })
        }

        <Grid item xs={12} p={5} alignSelf="flex-end">
            <Grid container justifyContent="space-between">
                <h2><b>Total for this order:</b></h2>
                <h2><Box sx={{display: "inline", fontSize: "0.5em"}}>&#8369;</Box><strong>{total}</strong></h2>
                <Button as={Link} to="/checkout"> Checkout </Button>
            </Grid>
        </Grid>

        </>
    )
}