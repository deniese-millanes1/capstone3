import { Grid, Card, Typography, CardContent, Box } from "@mui/material"
import { useEffect, useState } from "react"
import CardTypography1 from "./CardTypography1"
import ErrorOutlineIcon from '@mui/icons-material/ErrorOutline';
export default function OrderView() {
    const [orders, setOrders] = useState([]);

 
    useEffect(() => {
        fetch(`https://secure-castle-27864.herokuapp.com/users/orders`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            setOrders(data)

        })
    });
    

    return (
        <>
            {
                !orders ? 
                    <Grid item sx={{display: "flex"}}><ErrorOutlineIcon sx={{mr: 2, color: "#990f02"}}/><Typography variant="body1">You have no orders yet!</Typography></Grid>
                :
                <>
                {
                    orders.map((order) => {
                        return (
                            <Grid
                                item
                                xs={12}
                                sm={12}
                                md={12}
                                lg={6}
                                xl={6}
                                key={order._id}
                            >
                                <Card variant="outlined" sx={{ padding: 2 }}>
                                <CardContent>
                                    <Typography variant="h5" component="div" sx={{ overflow: 'auto', whiteSpace: "nowrap" }}>
                                    Order {order._id}
                                    </Typography>

                                    <h2>Ordered On:{order.orderedOn} </h2>
                                    <h1> Products: </h1>
                                        <Box sx={{ overflow: 'auto', height: "4rem"}}>
                                            {
                                                order.products.map((product) => {
                                                    return (
                                                        <Box mb={1} key={product.productId}>
                                                            <CardTypography1 props={{subtitle: "ID", content: product.productId}} />
                                                            <CardTypography1 props={{subtitle: "Quantity", content: product.quantity}} />
                                                            <CardTypography1 props={{subtitle: "Unit Price", content: product.price}} />
                                                        </Box>
                                                    )
                                                })
                                            }
                                        </Box>
                                        
                                    <h3>Total Price: {order.totalAmount} </h3>
                                </CardContent>
                                </Card>
                            </Grid>
                        )
                    })
                }
                </>
            }
        </>
    )
}