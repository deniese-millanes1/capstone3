import {Fragment, useState, useEffect, useContext} from "react";
import {Stack, Row, Card, Button} from "react-bootstrap";
import {Link} from 'react-router-dom';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import EditIcon from '@mui/icons-material/Edit';
import UserContext from "../UserContext";


export default function ProductView({ productViewProp }) {

    const { name, description, isActive, price, author, publishedOn, _id, imageURL, stocks } = productViewProp;
    console.log(productViewProp)
    const { user } = useContext(UserContext);
    console.log(isActive)

    const [readMore, setReadMore] = useState(false);
    const descriptionText = productViewProp.description
    const linkName = readMore ? 'Read Less << ' : 'Read More >> '