import {useState, useEffect} from "react";
import {Stack} from "react-bootstrap";
import {BrowserRouter as Router} from "react-router-dom";
import {Routes,Route, useNavigate} from "react-router-dom";
import {UserProvider} from "./UserContext";
import Header from './components/Header';
import Home from "./pages/user/Home";
import Login from "./pages/user/Login";
import Register from "./pages/user/Register";
import Logout from "./pages/user/Logout";
import Fixing from "./pages/user/Fixing";
import Products from "./pages/user/Products";
import AddToCart from "./pages/user/AddToCart";
import NotFound from "./pages/user/NotFound";
import AdminHome from "./pages/admin/AdminHome";
import ListProducts from "./pages/admin/ListProducts";
import NewProduct from "./pages/admin/NewProduct";
import UpdateProduct from "./pages/admin/UpdateProduct";
// import AdminProductView from "./components/AdminProductView";
import DeactivateProduct from "./pages/admin/DeactivateProduct";
import ReactivateProduct from "./pages/admin/ReactivateProduct";
import Cart from "./pages/user/Cart";
import Checkout from "./pages/user/Checkout";
import Orders from "./pages/user/Orders";
import './App.css';


function App() {

  const [user, setUser] = useState({
    //email: localStorage.getItem("email")
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear();
  }

  

  useEffect(() => {
  fetch("https://secure-castle-27864.herokuapp.com/users/details", {
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`
    }
  })
    .then(res => res.json())
    .then(data => {
      console.log(data)
      if(typeof data._id !== "undefined" && data.isAdmin === false) {
        console.log(user)
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
    } else if (typeof data._id !== "undefined" && data.isAdmin === true) {
        console.log(user)
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })

    } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  }, [])


  return (
    <UserProvider value={{user, setUser, unsetUser}} >
        <Router>
          <Stack gap={3} className="mx-auto">
            <Header />
              <Routes>
                  <Route exact path="/" element={<Home />} />
                  <Route exact path="/register" element={<Register />} />
                  <Route exact path="/login" element={<Login />} />
                  <Route exact path="/logout" element={<Logout />} />
                  <Route exact path="/books/best-seller" element={<Fixing />} />
                  <Route exact path="/books/new-releases" element={<Fixing />} />
                  <Route exact path="/genre" element={<Fixing />} />
                  <Route exact path="/books/exclusive-online-deals" element={<Fixing />} />
                  <Route exact path="/products" element={<Products />} />                  
                  <Route exact path="/products/:productId" element={<AddToCart />} />
                  <Route exact path="/admin" element={<AdminHome />} />
                  <Route exact path="/products/all" element={<ListProducts />} />
                  <Route exact path="/new-product" element={<NewProduct />} />
                  <Route exact path="/products/:productId/update" element={<UpdateProduct />} />
                  <Route exact path="/products/:productId/archive" element={<DeactivateProduct />} />
                  <Route exact path="/products/:productId/unarchive" element={<ReactivateProduct />} />
                  <Route exact path="/cart" element={<Cart />} />
                  <Route exact path="/checkout" element={<Checkout />} />
                  <Route exact path="/orders" element={<Orders />} />
                  <Route path="*" element={<NotFound />} />
              </Routes>
            </Stack>
        </Router>
    </UserProvider>  
  );
}

export default App;
